from tkinter import *


class MortgageCalculator:

    def __init__(self):
        window = Tk()
        window.title("Калькулятор ипотеки")
        Label(window, text="Годовая процентная ставка").grid(
            pady=5, padx=5, row=1, column=1, sticky=W
        )
        Label(window, text="%").grid(
            pady=5, padx=5, row=1, column=3, sticky=W
        )
        Label(window, text="Количество лет").grid(
            pady=5, padx=5, row=2, column=1, sticky=W
        )
        Label(window, text="Сумма кредита").grid(
            pady=5, padx=5, row=3, column=1, sticky=W
        )
        Label(window, text="Ежемесячный платеж").grid(
            pady=5, padx=5, row=4, column=1, sticky=W
        )
        Label(window, text="Общая сумма платежа").grid(
            pady=5, padx=5, row=5, column=1, sticky=W
        )

        self.annual_interest_rate = StringVar()
        Entry(
            window,
            textvariable=self.annual_interest_rate,
            justify=RIGHT
        ).grid(row=1, column=2)

        self.number_of_years = StringVar()
        Entry(
            window,
            textvariable=self.number_of_years,
            justify=RIGHT
        ).grid(row=2, column=2)

        self.loan_amount = StringVar()
        Entry(
            window,
            textvariable=self.loan_amount,
            justify=RIGHT
        ).grid(row=3, column=2)

        self.monthly_payment = StringVar(value='[Вычисляемое]     ')
        Label(
            window,
            textvariable=self.monthly_payment,
        ).grid(row=4, column=2, sticky=E)

        self.total_payment = StringVar(value='[Вычисляемое]     ')
        Label(
            window,
            textvariable=self.total_payment,
        ).grid(row=5, column=2, sticky=E)

        Button(
            window,
            text="Вычислить платеж",
            command=self.compute_payment
        ).grid(pady=10, row=6, column=2, sticky=E)
        window.mainloop()

    def compute_payment(self):
        loan_amount = float(self.loan_amount.get())
        annual_interest_rate = float(self.annual_interest_rate.get())
        number_of_years = int(self.number_of_years.get())

        monthly_payment = self.get_monthly_payment(
            loan_amount,
            annual_interest_rate,
            number_of_years,
        )
        monthly_payment = round(monthly_payment, 2)
        self.monthly_payment.set(str(monthly_payment))

        total_payment = self.get_total_payment(
            monthly_payment,
            number_of_years,
        )
        self.total_payment.set(format(total_payment, '10.2f'))

    @staticmethod
    def get_monthly_payment(loan_amount, annual_interest_rate, number_of_years):
        annual_interest_rate = annual_interest_rate / 1200
        return (loan_amount * annual_interest_rate
                / (1 - 1 / (1 + annual_interest_rate)
                   ** (number_of_years * 12)))

    @staticmethod
    def get_total_payment(monthly_payment, number_of_years):
        return float(monthly_payment) * 12 * int(number_of_years)


if __name__ == '__main__':
    MortgageCalculator()
