import unittest

from mortgage_calculator import MortgageCalculator


class TestCalculator(unittest.TestCase):
    def test_get_monthly_payment(self):
        monthly_payment = round(
            MortgageCalculator.get_monthly_payment(
                loan_amount=10_000_000,
                annual_interest_rate=10,
                number_of_years=3,
            ))
        self.assertEqual(monthly_payment, 322_672)

    def test_get_total_payment(self):
        total_payment = MortgageCalculator.get_total_payment(
            monthly_payment=322_672,
            number_of_years=3
        )
        self.assertEqual(round(total_payment), 11_616_192)
